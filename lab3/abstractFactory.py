from abc import ABCMeta, abstractmethod
from dataclasses import dataclass


@dataclass
class Customization:
    extraMilk: float
    sugar: float
    mugSize: float


@dataclass
class Preparation:
    milk: float
    water: float
    sugar: float
    coke: float
    liquidCoffee: float
    addedFlavour: float
    tea: float


class Product(metaclass=ABCMeta):
    @abstractmethod
    def make(self) -> None:
        pass


class Cappuccino(Product):
    def __init__(self, cust) -> None:
        self.cust = cust
        self.prep = Preparation(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

    def make(self) -> None:
        print(f"Cappuccino is made with {self.prep.milk}g milk, "
              f"{self.prep.sugar}g sugar and {self.prep.liquidCoffee}g coffee")
        print(f"Some customizations {self.cust.extraMilk}g extraMilk, "
              f"{self.cust.sugar}g sugar and {self.cust.mugSize}g mugSize")

    def set_milk(self, milk: float) -> None:
        self.prep.milk = milk

    def set_sugar(self, sugar: float) -> None:
        self.prep.sugar = sugar

    def set_coffee(self, coffee: float) -> None:
        self.prep.liquidCoffee = coffee


class BlackCoffee(Product):
    def __init__(self, custom) -> None:
        self.custom = custom
        self.prep = Preparation(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

    def make(self) -> None:
        print(f"BlackCoffee is made with "
              f"{self.prep.water}g water and {self.prep.liquidCoffee}g coffee")
        print(f"Some customizations {self.custom.extraMilk}g extraMilk, "
              f"{self.custom.sugar}g sugar and {self.custom.mugSize}g mugSize")

    def set_water(self, water: float) -> None:
        self.prep.water = water

    def set_coffee(self, coffee: float) -> None:
        self.prep.liquidCoffee = coffee


class Lemonade(Product):
    def __init__(self, custom) -> None:
        self.custom = custom
        self.prep = Preparation(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

    def make(self) -> None:
        print(f"Lemonade is made with {self.prep.water}g water, "
              f"{self.prep.sugar}g sugar and {self.prep.addedFlavour}g lemon juice")
        print(f"Some customizations {self.custom.extraMilk}g extraMilk, "
              f"{self.custom.sugar}g sugar and {self.custom.mugSize}g mugSize")

    def set_water(self, water: float) -> None:
        self.prep.water = water

    def set_sugar(self, sugar: float) -> None:
        self.prep.sugar = sugar

    def set_lemone_juice(self, lemon_juice: float) -> None:
        self.prep.addedFlavour = lemon_juice


class HotMilk(Product):
    def __init__(self, custom) -> None:
        self.custom = custom
        self.prep = Preparation(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

    def make(self) -> None:
        print(f"HotMilk is made with {self.prep.milk}g milk")
        print(f"Some customizations {self.custom.extraMilk}g extraMilk, "
              f"{self.custom.sugar}g sugar and {self.custom.mugSize}g mugSize")

    def set_milk(self, milk: float) -> None:
        self.prep.milk = milk


class CocaCola(Product):
    def __init__(self, custom) -> None:
        self.custom = custom
        self.prep = Preparation(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

    def make(self) -> None:
        print(f"CocaCola is made with {self.prep.water}g water and {self.prep.coke}g coke")
        print(f"Some customizations {self.custom.extraMilk}g extraMilk, "
              f"{self.custom.sugar}g sugar and {self.custom.mugSize}g mugSize")

    def set_water(self, water: float) -> None:
        self.prep.water = water

    def set_coke(self, coke: float) -> None:
        self.prep.coke = coke


class ProductFactory(metaclass=ABCMeta):
    @abstractmethod
    def get_product(self, customization: Customization) -> Product:
        pass

    @staticmethod
    def get_product_factory(factory_type):
        factory_dict = {
            "Cappuccino": CappuccinoFactory,
            "BlackCoffee": BlackCoffeeFactory,
            "Lemonade": LemonadeFactory,
            "HotMilk": HotMilkFactory,
            "CocaCola": CocaColaFactory
        }
        return factory_dict[factory_type]()


class CappuccinoFactory(ProductFactory):
    def get_product(self, customization: Customization) -> Cappuccino:
        return Cappuccino(customization)


class BlackCoffeeFactory(ProductFactory):
    def get_product(self, customization: Customization) -> BlackCoffee:
        return BlackCoffee(customization)


class LemonadeFactory(ProductFactory):
    def get_product(self, customization: Customization) -> Lemonade:
        return Lemonade(customization)


class HotMilkFactory(ProductFactory):
    def get_product(self, customization: Customization) -> HotMilk:
        return HotMilk(customization)


class CocaColaFactory(ProductFactory):
    def get_product(self, customization: Customization) -> CocaCola:
        return CocaCola(customization)


def main():
    # Cappuccino
    product = CappuccinoFactory().get_product_factory("Cappuccino")
    cappuccino = product.get_product(Customization(0.0, 0.0, 0.0))
    cappuccino.set_milk(100.0)
    cappuccino.set_coffee(0.5)
    cappuccino.set_sugar(0.25)
    cappuccino.make()

    print()

    # BlackCoffee
    product = BlackCoffeeFactory().get_product_factory("BlackCoffee")
    black_coffee = product.get_product(Customization(20.0, 1.0, 0.0))
    black_coffee.set_water(150.0)
    black_coffee.set_coffee(0.7)
    black_coffee.make()

    print()

    # Lemonade
    product = LemonadeFactory().get_product_factory("Lemonade")
    lemonade = product.get_product(Customization(0.0, 3.5, 1.0))
    lemonade.set_water(150.0)
    lemonade.set_sugar(2.5)
    lemonade.set_lemone_juice(5.5)
    lemonade.make()

    print()

    # HotMilk
    product = HotMilkFactory().get_product_factory("HotMilk")
    hot_milk = product.get_product(Customization(0.0, 1.5, 0.0))
    hot_milk.set_milk(150.0)
    hot_milk.make()

    print()

    # CocaCola
    product = CocaColaFactory().get_product_factory("CocaCola")
    coca_cola = product.get_product(Customization(0.0, 0.0, 50.0))
    coca_cola.set_water(145.0)
    coca_cola.set_coke(5.0)
    coca_cola.make()


if __name__ == "__main__":
    main()
