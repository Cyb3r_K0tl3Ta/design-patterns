from abc import ABCMeta, abstractmethod


class Appliance(metaclass=ABCMeta):
    @staticmethod
    @abstractmethod
    def enabled(self):
        pass

    @staticmethod
    @abstractmethod
    def start(self):
        pass

    @staticmethod
    @abstractmethod
    def stop(self):
        pass


class AC(Appliance):
    def __init__(self):
        self.enabled = False

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled: bool):
        self._enabled = enabled

    def start(self):
        print('___AC_ON_successfully___')
        self.enabled = True

    def stop(self):
        print('___AC_OFF_successfully___')
        self.enabled = False


class Refrigerator(Appliance):
    def __init__(self):
        self.enabled = False

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled: bool):
        self._enabled = enabled

    def start(self):
        print('___Refrigerator_ON_successfully___')
        self.enabled = True

    def stop(self):
        print('___Refrigerator_OFF_successfully___')
        self.enabled = False


class Fan(Appliance):
    def __init__(self):
        self.enabled = False

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled: bool):
        self._enabled = enabled

    def start(self):
        print('___Fan_ON_successfully___')
        self.enabled = True

    def stop(self):
        print('___Fan_OFF_successfully___')
        self.enabled = False


class TV(Appliance):
    def __init__(self):
        self.enabled = False

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled: bool):
        self._enabled = enabled

    def start(self):
        print('___TV_ON_successfully___')
        self.enabled = True

    def stop(self):
        print('___TV_OFF_successfully___')
        self.enabled = False


class GateOpener(Appliance):
    def __init__(self):
        self.enabled = False

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled: bool):
        self._enabled = enabled

    def start(self):
        print('___Gates_opened_successfully___')
        self.enabled = True

    def stop(self):
        print('___Gates_closed_successfully___')
        self.enabled = False

