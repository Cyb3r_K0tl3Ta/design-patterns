from appliance import *


class Switch(metaclass=ABCMeta):
    def __init__(self, appliance: Appliance):
        self.appliance = appliance

    @staticmethod
    @abstractmethod
    def turn_on(self):
        pass

    @staticmethod
    @abstractmethod
    def turn_off(self):
        pass


#Automatic Remote Controller
class ARC(Switch):
    def __init__(self, appliance: Appliance):
        super().__init__(appliance)

    def turn_on(self):
        print('ARC just start worked')
        print('ARC trying to turn on an appliance. LOADING.....\n')
        if self.appliance.enabled:
            print('Something get wrong, check manual.')
        else:
            self.appliance.start()

    def turn_off(self):
        print('ARC just start worked')
        print('ARC trying to turn on an appliance. LOADING.....\n')
        if self.appliance.enabled:
            self.appliance.start()
        else:
            print('Something get wrong, check manual.')

#Manual Remote Controller
class MRC(Switch):
    def __init__(self, appliance):
        super().__init__(appliance)

    def turn_on(self):
        print('MRC just start worked')
        print('MRC trying to turn on an appliance. LOADING.....\n')
        if self.appliance.enabled:
            print('Something get wrong, check manual.')
        else:
            self.appliance.start()

    def turn_off(self):
        print('MRC just start worked')
        print('MRC trying to turn on an appliance. LOADING.....\n')
        if self.appliance.enabled:
            self.appliance.start()
        else:
            print('Something get wrong, check manual.')
