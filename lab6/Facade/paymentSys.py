from shipmentSys import *

class Payment:
    def __init__(self):
        self.card_details = {}

    def add_card_details(self, card_number, money) -> None:
        self.card_details = {'card_number': card_number, 'balance': money}

    def verify_payment(self, shopping_cart: ShoppingCart):
        return shopping_cart.checkout(self.card_details['balance'])
