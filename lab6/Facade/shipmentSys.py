from orderProc import *


class Shipment:
    def __init__(self, order: Order, provider):
        self.order = order
        self.provider = provider

    def create_shipment(self, provider_id: int, product_price: float, stock: Stock) -> None:
        for i in range(len(self.provider.providers)):
            if provider_id == self.provider.id:
                self.order.create_order(stock, product_price)
                print(f"SHIPMENT. Your order with id={self.order.id} has been delivered")

    def add_provider(self, provider_id: int):
        self.provider.add_provider(provider_id)


class ShipmentProvider:
    def __init__(self, id: int, name: str, phone_number: str):
        self.id = id
        self.name = name
        self.phone_number = phone_number
        self.providers = []

    def add_provider(self) -> None:
        print(f"Provider with id={self.id} has been added")
        self.providers.append({'id': self.id,
                               'name': self.name,
                               'email': self.phone_number})

'''    def change_provider(self, provider_id: int, product_price: float, stock: Stock) -> None:
        for x in range(len(self.provider_id.providers)):
            if provider_id == self.provider.id:
                self.order.create_order(stock, product_price)
                print(f"Your order with id={self.order.id} has been delivered")'''