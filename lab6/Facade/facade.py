from paymentSys import *

class OrderFacade:
    def __init__(self, stock: Stock, shipment_provider: ShipmentProvider, shopping_cart: ShoppingCart):
        self.shipment_provider = shipment_provider
        self.stock = stock
        self.shopping_cart = shopping_cart

    def do_operation(self, order_id, provider_id, product_price) -> None:
        order = Order(order_id)
        shipment = Shipment(order, self.shipment_provider)
        order.create_order(self.stock, product_price)
        shipment.create_shipment(provider_id, self.shopping_cart.total, self.stock)
        print(f"You must pay {product_price}$")


class Customer:
    def __init__(self, payment: Payment):
        self.payment = payment

    def order_item(self, order_id, provider_id, product_price, order_facade: OrderFacade) -> None:
        print('You have to input you card number OR input [x] to exit:')
        while True:
            card_number = str(input('> '))
            if card_number == self.payment.card_details['card_number']:
                break
            elif card_number == 'x':
                return
        if self.payment.verify_payment(order_facade.shopping_cart) >= product_price:
            order_facade.do_operation(order_id, provider_id, product_price)
            print(f"Balance: {self.payment.card_details['balance']}$")
            self.payment.card_details['balance'] -= product_price
            print(f"-{product_price}$\nBalance: {self.payment.card_details['balance']}$")
        else:
            print(f"You must pay {product_price}$, you lack {product_price-self.payment.card_details['balance']}$")
