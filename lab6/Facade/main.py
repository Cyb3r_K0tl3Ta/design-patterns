from facade import *


product1 = Product(product_id=1, price=167.0, type='condoms')
product2 = Product(product_id=2, price=124.0, type='liquid')

stock = Stock(stock_id=1)
stock.update_stock(product=product1, amount=10)
stock.select_stock_item(product=product1)
stock.update_stock(product=product2, amount=34)
stock.select_stock_item(product=product2)

shopping_cart = ShoppingCart(stock=stock)

shipment_provider = ShipmentProvider(id=1, name='Alex', phone_number='067-21-82-939')
shipment_provider.add_provider()
order_facade = OrderFacade(stock=stock, shipment_provider=shipment_provider, shopping_cart=shopping_cart)

payment = Payment()
payment.add_card_details(card_number='4423-3252-2312-4765', money=2476.67)
print(payment.card_details)

customer = Customer(payment=payment)
customer.order_item(order_id=1,
                    provider_id=shipment_provider.id,
                    product_price=product1.price+product2.price,
                    order_facade=order_facade)
