from inventorySys import *


class ShoppingCart:
    def __init__(self, stock: Stock):
        self.stock = stock
        self.total = 0.0
        self.items = {}

    def add_item(self, product_price) -> None:
        self.total += (self.stock.amount * product_price)

    def update_amount(self, stock_id, new_amount: float) -> None:
        self.items = {stock_id: new_amount}

    def checkout(self, cash_paid: float):
        if cash_paid >= self.total:
            return cash_paid - self.total
        return 'Cash paid not enough'


class Order:
    def __init__(self, order_id: int):
        self.id = order_id
        self.shopping_cart = None

    def create_order(self, stock, product_price):
        self.shopping_cart = ShoppingCart(stock)
        self.shopping_cart.add_item(product_price)

    def edit_order(self, stock_id, amount, shopping_cart: ShoppingCart):
        shopping_cart.update_amount(stock_id, amount)
