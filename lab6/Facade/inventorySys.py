class Product:
    def __init__(self, product_id: int, price: float, type: str):
        self.id = product_id
        self.price = price
        self.type = type

    def add_product(self, id: int, price: float, type: str) -> None:
        self.id = id
        self.price = price
        self.type = type
        print(f"Product with id={self.id} is added")

    def update_product(self):
        return f"Product[id={self.id}, price={self.price}, type={self.type}]"


class Stock:
    def __init__(self, stock_id: int):
        self.id = stock_id
        self.amount = 0
        self.products = []

    def select_stock_item(self, product) -> None:
        for i in range(len(self.products)):
            if product.id == self.products[i]['id']:
                print(f"Stock[id={self.id}, amount={self.amount}, {product.update_product()}]")

    def update_stock(self, product: Product, amount: int):
        self.amount = amount
        product.add_product(product.id, product.price, product.type)
        self.products.append({"id": product.id, "price": product.price, "type:": product.type})