from idb import *


class ATM:
    def __init__(self, database_proxy: DatabaseProxy):
        self.dbp = database_proxy

    def handle_balance_request(self) -> int:
        return self.dbp.real_account.get_balance()

    def handle_login(self, account_id: str) -> IAcct:
        return self.dbp.login(account_id)

    def handle_logout(self):
        return self.dbp.logout(self.dbp.real_account)

    def handle_deposit(self, amount: int) -> bool:
        return self.dbp.real_account.deposit(amount)

    def handle_withdrawal(self, amount: int, customer):
        return customer.withdraw(amount, self.dbp)