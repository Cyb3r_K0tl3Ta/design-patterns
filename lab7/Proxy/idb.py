from iAcc import *


class IDB(metaclass=ABCMeta):
    @abstractmethod
    def login(self, account_id: str) -> IAcct:
        pass

    @abstractmethod
    def logout(self, a: IAcct):
        pass


class AccountDatabase(IDB):
    def login(self, account_id: str):
        print("You have logged in successfully!")
        return Account(account_id)

    def logout(self, a: IAcct):
        print("You have logged out successfully!")


class DatabaseProxy(IDB):
    def __init__(self, real_database: AccountDatabase):
        self.real_database = real_database
        self.real_account = None

    def login(self, account_id: str) -> IAcct:
        self.real_account = self.real_database.login(account_id)
        balance = self.real_account.get_balance()
        return self.make_account_proxy(balance)

    def logout(self, a: IAcct):
        self.real_account.balance = a.get_balance()
        self.real_database.logout(self.real_account)

    def make_account_proxy(self, start_amount: int):
        return AccountProxy(start_amount)

