from customer import *


db = AccountDatabase()
dbp = DatabaseProxy(db)
atm = ATM(dbp)
customer = Customer(id=1, name='Mike', account_number='223-234-769')
login = str(input("Do you want to log in [y/n]: "))
if login == 'y':
    an = str(input('Enter your account number: '))
    if an == customer.account_number:
        atm.handle_login(account_id='1')
        dbp.real_account.attach_customer(customer=customer)
        print("--OPTIONS--")
        print("1 - check balance")
        print("2 - deposit money")
        print("3 - withdraw money")
        print("4 - log out")
        choice = 0
        while choice != 4:
            choice = int(input('> '))
            if choice == 1:
                print(f"Balance: {atm.handle_balance_request()}$")
            elif choice == 2:
                amount = int(input('Request amount of money to deposit: '))
                print(f"+{amount}$\n{atm.handle_deposit(amount)}")
            elif choice == 3:
                amount = int(input('Request amount of money to withdrawal: '))
                print(f"-{amount}$\n{atm.handle_withdrawal(amount, customer)}")
        dbp.real_account.detach_customer(customer)
        atm.handle_logout()
elif login == 'n':
    print("OK")
