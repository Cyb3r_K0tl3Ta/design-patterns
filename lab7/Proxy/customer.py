from atm import *


class Customer:
    def __init__(self, id: int, name: str, account_number: str):
        self.id = id
        self.name = name
        self.account_number = account_number

    def notify(self, account_proxy: AccountProxy):
        account_proxy.notify()
        print(f"Customer[id={self.id}, name={self.name}, account_number={self.account_number}]")

    def withdraw(self, amount: int, dbp: DatabaseProxy):
        return dbp.real_account.withdraw(amount)
