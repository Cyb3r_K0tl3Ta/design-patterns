from abc import ABCMeta, abstractmethod


class IAcct(metaclass=ABCMeta):
    @abstractmethod
    def get_balance(self) -> int:
        pass

    @abstractmethod
    def withdraw(self, amount: int) -> bool:
        pass

    @abstractmethod
    def attach_customer(self, customer) -> None:
        pass

    @abstractmethod
    def detach_customer(self, customer) -> None:
        pass

    @abstractmethod
    def notify(self) -> None:
        pass


class AccountProxy(IAcct):
    def __init__(self, balance: int):
        self._balance = balance
        self._customers = []
        self.account = Account(self)

    def get_balance(self) -> int:
        self.notify()
        return self.account.get_balance()

    def deposit(self, amount: int) -> bool:
        return self.account.deposit(amount)

    def withdraw(self, amount: int) -> bool:
        return self.account.withdraw(amount)

    def attach_customer(self, customer) -> None:
        self.account.attach_customer(customer)
        self._customers.append(customer)

    def detach_customer(self, customer) -> None:
        self.account.detach_customer(customer)
        self._customers.remove(customer)

    def notify(self) -> None:
        self.account.notify()
        for customer in self._customers:
            customer.notify(self)


class Account(IAcct):
    def __init__(self, account_id):
        self.account_id = account_id
        self.balance = 0

    def get_balance(self) -> int:
        return self.balance

    def deposit(self, amount: int) -> bool:
        if amount > 0:
            self.balance += amount
            return True
        else:
            return False

    def withdraw(self, amount: int) -> bool:
        if amount <= self.balance:
            self.balance -= amount
            return True
        else:
            return False

    def attach_customer(self, customer) -> None:
        print(f"IAcct: Attached a customer with id={customer.id}.")

    def detach_customer(self, customer) -> None:
        print(f"IAcct: Detached a customer with id={customer.id}.")

    def notify(self) -> None:
        print("IAcct: Notifying customers...")

