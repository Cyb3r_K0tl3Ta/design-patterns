from moveRole import *


knight_move = KnightMove(position=[(1, 0)])
knight = Knight(position=knight_move.position, color='White', move_rule=knight_move)
knight.print_info()
print(f"Available moves: {knight_move.get_all_moves()}")
coord_x = int(input('x > '))
coord_y = int(input('y > '))
print(knight.move([(coord_x, coord_y)]))
print("----------------------------------------------------------------------------")
bishop_move = BishopMove(position=[(2, 7)])
bishop = Bishop(position=bishop_move.position, color='Black', move_rule=bishop_move)
bishop.print_info()
print(f"Available moves: {bishop_move.get_all_moves()}")
coord_x = int(input('x > '))
coord_y = int(input('y > '))
print(bishop.move([(coord_x, coord_y)]))


